package main

type X struct {
	i int
}

func (x X) Method() {

}

type Iface interface {
	Method()
}

/* what effectively happens:
    v := X{}
	var x Iface
	x.Type = _type_X
	x.Value = new(X) // It is this new(X) that escapes
	*x.Value = v
	(*x.Value).Method()
	sink = (*x.Value)
*/
func main() {
	v := X{}
	var x Iface = v
	x.Method() // BAD: makes interface backing storage escape
	var sink interface{}
	sink = x.(X) // BAD: makes interface backing storage escape
	_ = sink
}

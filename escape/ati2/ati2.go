package main

type X struct {
	p *int
}

func main() {
	i := 0
	var v X
	v.p = &i // i does not escape
	y := new(X)
	y.p = &i // BAD: i escapes
}

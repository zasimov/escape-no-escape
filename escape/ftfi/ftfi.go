package main

type X struct {
	bar int
	foo *int
}

func f() int {
	i := 0
	var x X
	x.foo = &i
	return x.bar // BAD: &i escapes
}

func main() {
	i := f()
	_ = i
}

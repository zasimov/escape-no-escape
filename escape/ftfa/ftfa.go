package main

type X struct {
	name string
}

func (x *X) SetName(s string) {
	x.name = s // BAD: s contents escape to sink
}

func main() {
	var x X
	x.SetName("string")
}

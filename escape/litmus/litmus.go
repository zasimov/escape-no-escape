package main

import (
	"bytes"
	"fmt"
)

func f1() {
	i := 0
	var sink interface{}
	sink = fmt.Sprint(&i)
	_ = sink
}

func f2() {
	var buf bytes.Buffer
	buf.Write([]byte{1})
	_ = buf.Bytes()
}

func main() {
	f1()
	f2()
}

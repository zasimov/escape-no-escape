package main

type T struct {
	p int
}

func main() {
	m := make(map[int]*T) // the map does not escape
	m[0] = new(T)         // BAD: new(T) escapes
}

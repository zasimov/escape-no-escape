package main

type Y struct {
	j int
}

type X struct {
	f Y
}

func f() *X {
	var v X
	p := &Y{} // BAD: &Y{} escapes
	v.f = *p
	return &v
}

func main() {
	v := f()
	_ = v
}

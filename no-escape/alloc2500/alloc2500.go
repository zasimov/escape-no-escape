package main

type S struct {
	i int
	f float32
	d float64
}

func main() {
	var s []S
	s = make([]S, 64, 2500)
	s[14] = S{1, 1.0, 1.0}
}

package main

func noescape(y ...interface{}) {
}

func main() {
	x := 0 // BAD: x escapes
	noescape(&x)
}

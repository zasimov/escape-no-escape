package main

func main() {
	var y int // BAD: y escapes
	func(p *int, x int) {
		*p = x
	}(&y, 42)

	x := 0 // BAD: x escapes
	defer func(p *int) {
		*p = 1
	}(&x)
}

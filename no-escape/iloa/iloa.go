package main

func foo(p **int) {
	var sink interface{}
	sink = *p
	_ = sink
}

func main() {
	x := new(int)
	foo(&x) // BAD: x escapes
}

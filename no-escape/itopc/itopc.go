package main

func foo(p **int) *int {
	return *p
}

func main() {
	x := new(int) // BAD: new(int) escapes
	_ = foo(&x)
}

package main

func main() {
	var x []int
	x = append(x, 1, 2, 3) // allocates a non-escaping array
	x = append(x, 4)       // reallocates a non-escaping array
	_ = x
}

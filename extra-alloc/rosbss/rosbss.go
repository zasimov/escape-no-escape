package main

func f() string {
	var b []byte
	b = append(b, "Hello, "...)
	b = append(b, "world.\n"...)
	return string(b) // BAD: allocates again
}

func main() {
	s := f()
	_ = s
}

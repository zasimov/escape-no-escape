package main

import "bytes"

func isWS(b byte) bool {
	// BAD: allocate buffer for byte slice
	return bytes.IndexByte([]byte("\t\n\x0C\n "), b) != -1
}

func main() {
	b := isWS(' ')
	_ = b
}

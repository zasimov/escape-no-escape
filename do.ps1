function Run($parent, $child) {
    $path = Join-Path -Path $parent -ChildPath $child
    Write-Host "========================================================="
    Write-Host "($parent)"
    Get-Content (Join-Path -Path $path -ChildPath README.md)
    Write-Host "========================================================="
    Write-Host
    $go = Join-Path -Path $path -ChildPath "$child.go"
    Get-Content $go
    Write-Host
    go run -gcflags "-m -l" $go
    Write-Host
    Write-Host
}

Run "escape" "ati1"
Run "escape" "ati2"
Run "escape" "atsam"
Run "escape" "ftfa"
Run "escape" "iliwfa"
Run "escape" "iliwfooi"
Run "escape" "ftfi"
Run "escape" "litmus"

Run "extra-alloc" "rosbss"
Run "extra-alloc" "robs"

Run "no-escape" "closure-calls"
Run "no-escape" "dot-dot-dot"
Run "no-escape" "itopc"
Run "no-escape" "iloa"
Run "no-escape" "append"
Run "no-escape" "alloc2500"
